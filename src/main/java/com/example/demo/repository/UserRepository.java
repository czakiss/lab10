package com.example.demo.repository;

import com.example.demo.model.User;

import java.util.HashMap;
import java.util.Map;

public class UserRepository {

    private final int BLOCK_NUMBER = 2;
    private final int DEFAULT_NUMBER = 0;

    private final Map<Integer, User> usersDatabase;

    public UserRepository() {
        usersDatabase = new HashMap<>();

        usersDatabase.put(1, new User("cracker", "cracker1234", true, 0));
        usersDatabase.put(2, new User("marry", "marietta!#09", true, 0));
        usersDatabase.put(3, new User("silver", "$silver$", true, 0));
    }

    public enum StatusLogin{
        OK,
        FORBIDDEN,
        UNAUTHORIZED
    }

    public StatusLogin checkLogin(final String login, final String password) {
        // TODO: Prosze dokonczyc implementacje...

        for (User user : usersDatabase.values()) {
            if(user.getLogin().equals(login)){
                System.out.println(user.toString());
                if  (user.getPassword().equals(password)){
                    user.setIncorrectLoginCounter(DEFAULT_NUMBER);
                    return StatusLogin.OK;
                }
                int incorrectLoginCounter = user.getIncorrectLoginCounter();

                if(incorrectLoginCounter >= BLOCK_NUMBER && user.isActive()){
                    user.setActive(false);
                }
                else if(!user.isActive()){
                    return StatusLogin.FORBIDDEN;
                } else {
                    user.setIncorrectLoginCounter(incorrectLoginCounter+1);
                }

            }

        }
        return StatusLogin.UNAUTHORIZED;
    }
}
